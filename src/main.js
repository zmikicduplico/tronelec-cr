const { app, BrowserWindow, Tray, ipcMain, Notification } = require("electron");
const path = require("path");
const Mousetrap = require("mousetrap");

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require("electron-squirrel-startup")) {
  // eslint-disable-line global-require
  app.quit();
}

// Notifications title set to App name
if (process.platform === "win32") {
  app.setAppUserModelId(app.name);
}

let mainWindow;
const iconPath = path.join(__dirname, "./img/rfid.png");
let appIcon = null;

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    //width: 880,
    //height: 800,
    //x: 350,
    //y: 300,
    //    fullscreen: true,
    kiosk: true,
    icon: __dirname + "/img/rfid.png",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      //enableRemoteModule: true,
      worldSafeExecuteJavaScript: true,
    },
  });

  appIcon = new Tray(iconPath);
  appIcon.setToolTip("Duplico RFID");
  appIcon.on("click", () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show();
  });

  // hide menu bar
  mainWindow.setMenuBarVisibility(false);
  // and load the login.html of the app.
  mainWindow.loadFile(path.join(__dirname, "index.html"));
  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  // add notification for Login
  const NOTIFICATION_TITLE = "Duplico RFID Login";
  const NOTIFICATION_BODY = "Uspjesno ste se ulogirali!";

  function showNotification() {
    new Notification({
      title: NOTIFICATION_TITLE,
      body: NOTIFICATION_BODY,
    }).show();
  }

  ipcMain.on("notify", (_, message) => {
    showNotification();
    console.log(message);
    mainWindow.hide();
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
  createWindow();
  //  createSystemTray();
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
