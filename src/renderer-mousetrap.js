const { ipcRenderer } = require("electron");
const Mousetrap = require("mousetrap");

let message = "Uspješno ste logirani!";
/*
let open = "o p e n enter";
Mousetrap.bind(`${open}`, function () {
  ipcRenderer.send("notify", message);
});
*/
Mousetrap.bind("o p e n enter", function () {
  console.log("Uspjesno ste logirani!");
  ipcRenderer.send("notify", message);
});

Mousetrap.bind("0 9 8 3 3 0 2 8 enter", function () {
  console.log("Uspjesno ste logirani!");
  ipcRenderer.send("notify", message);
});

Mousetrap.bind("0 9 8 3 4 5 6 4 enter", function () {
  console.log("Uspjesno ste logirani!");
  ipcRenderer.send("notify", message);
});

Mousetrap.bind("h e l l o enter", function () {
  console.log("Uspjesno ste logirani!");
  ipcRenderer.send("notify", message);
});

Mousetrap.bind("3 0 2 6 4 8 1 8 6 3 enter", function () {
  console.log("Uspjesno ste logirani!");
  ipcRenderer.send("notify", message);
});

Mousetrap.bind("8 7 4 9 2 9 1 0 7 enter", function () {
  console.log("Uspjesno ste logirani!");
  ipcRenderer.send("notify", message);
});

Mousetrap.bind("1 2 3 4 enter", function () {
  console.log("Uspjesno ste logirani!");
  ipcRenderer.send("notify", message);
});
