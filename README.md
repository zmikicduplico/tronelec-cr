# Tronelec Six - RF_ID reader elec_tron app
- desktop app powered with RF_ID reader

## Install
- npm install (or use yarn install)
- npm run dev


## Packaging (win-x64)
npx electron-packager . <APP-NAME> --platform=win32 --arch=x64

